$(document).ready(function() {
	$("#datosACargar").html('<img src="loadingAnimation.gif" alt="cargando" />');
	setTimeout(obtenerDatosMasivos, 300);
});

function obtenerDatosMasivos()
{
	$.ajax({
		url: 'obtenerDatosMasivos.php',
		type: 'POST'
	})
	.done(function(data) {
		if (data.error == 0)
		{
			var string = '';
			$.each(data.asistencias, function(key, elem) {
				string += '<div>' + elem + '</div>';
			});
			$("#datosACargar").html(string);
		}
		else
		{
			alert('error');
		}
	})
	.fail(function() {
		alert('error');
	});
}